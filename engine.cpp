#include "engine.hpp"

namespace {
    constexpr auto DEFAULT_OBJECT_POOL_SIZE = 1000000 + 1; // 10^5 vertices
}

Engine::Engine() : m_used_ids{} {
// preallocate memory for vertices and ids
    m_used_ids.resize(DEFAULT_OBJECT_POOL_SIZE);
    std::fill(std::begin(m_used_ids), std::begin(m_used_ids) + DEFAULT_OBJECT_POOL_SIZE, false);
    m_vertices.reserve(DEFAULT_OBJECT_POOL_SIZE);
    m_edges.reserve(DEFAULT_OBJECT_POOL_SIZE);
}

Vertex &Engine::createVertex(const std::string &label) {
    auto &vertex = createVertex();
    vertex.addLabel(label);
    return vertex;
}

Vertex &Engine::createVertex() {
    const auto it = std::ranges::find_if(m_used_ids, [](const bool id) { return id == false; });
    const auto id = std::distance(std::begin(m_used_ids), it);
    *it = true;
    m_vertices.emplace_back(Vertex(id));
    return m_vertices.back();
}

void Engine::createEdge(Vertex &a, const Vertex &b) {
    const auto edge_iter = std::ranges::find_if(m_edges,
                                                [&](const Edge &edge) { return a.getId() == edge.vertex_id; });
    if (edge_iter == std::end(m_edges)) {
        // edge exists for a but doesn't have b as neighbor yet
        a.addNeighbor(b);
        m_edges.emplace_back(Edge{a.getId(), {b.getId()}});
        return;
    }
    const auto neighbor_iter = std::ranges::find_if(edge_iter->neighbor_vertex_ids,
                                                    [&](const std::size_t neighbor_id) {
                                                        return b.getId() == neighbor_id;
                                                    });
    if (neighbor_iter != std::end(edge_iter->neighbor_vertex_ids)) {
        // this edge already exists, do nothing
        return;
    }
    edge_iter->neighbor_vertex_ids.push_back(b.getId());
    a.addNeighbor(b);
}


std::vector<std::size_t> Engine::shortestPath(const Vertex &a, const Vertex &b, const std::string &label) const {
    std::vector<bool> visited;
    std::vector<std::size_t> parents;
    std::deque<const Vertex *> queue;
    visited.resize(m_used_ids.size());
    parents.resize(m_used_ids.size());
    std::fill(std::begin(visited), std::end(visited), false);
    visited[a.getId()] = true;
    queue.push_back(&a);
    while (!queue.empty()) {
        const auto &current_node = queue.front();
        queue.pop_front();
        auto contains_label = [&](const Vertex *vertex) {
            const auto &labels = vertex->getLabels();
            return std::find(std::begin(labels), std::end(labels), label) != std::end(labels);
        };
        for (const auto &neighbor : current_node->getNeighbors() | std::views::filter(contains_label)) {
            if (visited[neighbor->getId()] == false) {
                visited[neighbor->getId()] = true;
                parents[neighbor->getId()] = current_node->getId();
                queue.push_back(neighbor);
                if (neighbor->getId() == b.getId()) {
                    parents[neighbor->getId()] = current_node->getId();
                    break;
                }
            }
        }
    }
    return reconstructPath(a, b, parents);
}

std::vector<size_t>
Engine::reconstructPath(const Vertex &a, const Vertex &b, const std::vector<std::size_t> &parents) const {
    std::vector<std::size_t> ids_of_shortest_path;
    std::size_t current_node_id = b.getId();
    while (true) {
        ids_of_shortest_path.push_back(current_node_id);
        current_node_id = parents[current_node_id];

        if (current_node_id == 0) // no parent, that's the root
        {
            break;
        }
    }
    ids_of_shortest_path.push_back(a.getId());
    return ids_of_shortest_path;
}