#ifndef GRAPH_VERTEX_HPP
#define GRAPH_VERTEX_HPP

#include <memory>
#include <vector>

class Vertex {
public:

    Vertex(const std::string &label, std::size_t id) : Vertex(id) {
        m_labels.push_back(label);
    }

    Vertex(std::size_t id) : m_id(id) {}

    void addNeighbor(const Vertex &neighbor) {
        if (neighbor.getId() == getId()) {
            return;
        }
        m_neighbors.push_back(&neighbor);
    }

    void addLabel(const std::string &label);

    void removeLabel(const std::string &label) {
        m_labels.erase(std::remove(std::begin(m_labels), std::end(m_labels), label), std::end(m_labels));
    }

    [[nodiscard]] std::size_t getId() const {
        return m_id;
    }

    [[nodiscard]] auto &getNeighbors() const {
        return m_neighbors;
    }

    [[nodiscard]] const auto &getLabels() const {
        return m_labels;
    }

private:
    std::size_t m_id;
    std::vector<const Vertex *> m_neighbors;
    std::vector<std::string> m_labels;
};

#endif //GRAPH_VERTEX_HPP
