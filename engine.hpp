#ifndef GRAPH_ENGINE_HPP
#define GRAPH_ENGINE_HPP

#include <algorithm>
#include <deque>
#include <ranges>
#include "vertex.hpp"

struct Edge {
    std::size_t vertex_id = {};
    std::vector<std::size_t> neighbor_vertex_ids = {};
};


class Engine {
public:
    Engine();

    ~Engine() = default;

    Vertex &createVertex(const std::string &label);

    Vertex &createVertex();

    void createEdge(Vertex &a, const Vertex &b);

    void addLabel(Vertex &a, const std::string &label) {
        a.addLabel(label);
    }

    void removeLabel(Vertex &a, const std::string &label) {
        a.removeLabel(label);
    }

    [[nodiscard]] std::vector<bool> &getUsedIds() { return m_used_ids; }

    std::vector<std::size_t> shortestPath(const Vertex &a, const Vertex &b, const std::string &label) const;

    std::vector<size_t>
    reconstructPath(const Vertex &a, const Vertex &b, const std::vector<std::size_t> &parents) const;

private:
    std::vector<bool> m_used_ids;
    std::vector<Vertex> m_vertices;
    std::vector<Edge> m_edges;
};


#endif //GRAPH_ENGINE_HPP
