#include "vertex.hpp"

void Vertex::addLabel(const std::string &label) {
    // check if label exists already, if it does, then skip the insertion
    if (std::find(std::begin(m_labels), std::end(m_labels), label) != std::end(m_labels)) {
        return;
    }
    m_labels.push_back(label);
}