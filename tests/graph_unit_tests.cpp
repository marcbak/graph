#include <gtest/gtest.h>
#include "../engine.hpp"

class EngineFixture: public ::testing::Test{
public:
    EngineFixture() = default;

protected:
    Engine m_engine;
};

TEST_F(EngineFixture, ShouldCorrectlySetIdForTwoNodes)
{
    auto vertex_a = m_engine.createVertex();
    auto vertex_b = m_engine.createVertex();
    ASSERT_EQ(vertex_a.getId(), 0);
    ASSERT_EQ(vertex_b.getId(), 1);
}

TEST_F(EngineFixture, ShouldCorrectlyFillInternalIdMap)
{
    auto vertex_a = m_engine.createVertex();
    auto vertex_b = m_engine.createVertex();
    const auto used_ids = m_engine.getUsedIds();
    ASSERT_EQ(true, used_ids[0]);
    ASSERT_EQ(true, used_ids[1]);
}

TEST_F(EngineFixture, ShouldCorrectlyAddLabelToVertex)
{
    const std::string FIRST_LABEL = "this_is_a_label";
    const std::string SECOND_LABEL = "this_is_a_second_label";
    auto vertex = m_engine.createVertex();
    m_engine.addLabel(vertex, FIRST_LABEL);
    m_engine.addLabel(vertex, SECOND_LABEL);

    // we add the first label for the second time but we don't want duplicates
    m_engine.addLabel(vertex, FIRST_LABEL);
    const auto& labels = vertex.getLabels();
    ASSERT_EQ(labels.size(), 2);
    ASSERT_NE(std::end(labels), std::find(std::begin(labels), std::end(labels), FIRST_LABEL));
    ASSERT_NE(std::end(labels), std::find(std::begin(labels), std::end(labels), SECOND_LABEL));
}

TEST_F(EngineFixture, ShouldCorrectlyRemoveLabelFromVertex)
{
    const std::string FIRST_LABEL = "this_is_a_label";
    const std::string SECOND_LABEL = "this_is_a_second_label";
    auto vertex = m_engine.createVertex();
    m_engine.addLabel(vertex, FIRST_LABEL);
    m_engine.addLabel(vertex, SECOND_LABEL);
    const auto& labels = vertex.getLabels();
    ASSERT_EQ(labels.size(), 2);
    m_engine.removeLabel(vertex, FIRST_LABEL);
    ASSERT_EQ(labels.size(), 1);
}

TEST_F(EngineFixture, ShouldCorrectlyAddEdgesBetweenVertices)
{
    auto vertex_a = m_engine.createVertex();
    auto vertex_b = m_engine.createVertex();
    auto vertex_c = m_engine.createVertex();
    m_engine.createEdge(vertex_a, vertex_b);
    m_engine.createEdge(vertex_b, vertex_c);
    ASSERT_EQ(vertex_a.getNeighbors().size(), 1);
    ASSERT_EQ(vertex_b.getNeighbors().size(), 1);
    ASSERT_EQ(vertex_a.getNeighbors()[0]->getId(), vertex_b.getId());
    ASSERT_EQ(vertex_b.getNeighbors()[0]->getId(), vertex_c.getId());
}

TEST_F(EngineFixture, ShouldFindTheClosestPathBetweenSmallGraph)
{
    const auto label = "label";
    std::vector<Vertex *> vertices;
    for (int i = 0; i < 5; i++) {
        vertices.emplace_back(&m_engine.createVertex(label));
    }
    m_engine.createEdge(*vertices[0], *vertices[1]);
    m_engine.createEdge(*vertices[0], *vertices[2]);
    m_engine.createEdge(*vertices[2], *vertices[3]);
    m_engine.createEdge(*vertices[2],*vertices[4]);
    m_engine.createEdge(*vertices[0], *vertices[4]);
    const auto ids = m_engine.shortestPath(*vertices[0], *vertices[4], "label");
    const std::vector<std::size_t> expected_ids{4, 0};
    ASSERT_EQ(ids, expected_ids);
}

TEST_F(EngineFixture, ShouldFindClosestPathBetweenMediumSizedGraph)
{
    const auto label = "label";
    std::vector<Vertex *> vertices;
    for (int i = 0; i < 9; i++) {
        vertices.emplace_back(&m_engine.createVertex(label));
    }
    m_engine.createEdge(*vertices[0], *vertices[1]);
    m_engine.createEdge(*vertices[0], *vertices[2]);
    m_engine.createEdge(*vertices[0], *vertices[3]);
    m_engine.createEdge(*vertices[2], *vertices[4]);
    m_engine.createEdge(*vertices[2], *vertices[5]);
    m_engine.createEdge(*vertices[4], *vertices[6]);
    m_engine.createEdge(*vertices[4], *vertices[7]);
    m_engine.createEdge(*vertices[7], *vertices[8]);
    m_engine.createEdge(*vertices[3], *vertices[8]);
    const auto ids = m_engine.shortestPath(*vertices[0], *vertices[8], "label");
    const std::vector<std::size_t> expected_ids{8, 3, 0};
    ASSERT_EQ(ids, expected_ids);

}
