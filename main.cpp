#include <iostream>
#include <random>
#include <chrono>
#include "engine.hpp"

namespace {
    constexpr auto NUM_OF_VERTICES = 100000;
    constexpr auto NUM_OF_SHORTEST_PATH_QUERIES = 100;
    constexpr auto MAX_NUMBER_OF_NEIGHBORS = 100; // for easier testing
    const std::vector<std::string> labels{"a", "b", "c", "d", "e"};
}

int main() {

    Engine dbEngine;
    std::vector<Vertex *> vertices;
    vertices.reserve(NUM_OF_VERTICES);
    std::cout << "creating vertices" << std::endl;
    for (int i = 0; i < NUM_OF_VERTICES; i++) {
        auto &vertex = dbEngine.createVertex();
        dbEngine.addLabel(vertex, labels[i % labels.size()]);
        vertices.push_back(&vertex);
    }
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> distribution(0, NUM_OF_VERTICES - 1);
    std::cout << "creating edges" << std::endl;

    for (int i = 0; i < NUM_OF_VERTICES; i++) {
        const auto first_index = distribution(rng);
        for (int j = 0; j < MAX_NUMBER_OF_NEIGHBORS; j++) {
            const auto second_index = distribution(rng);
            dbEngine.createEdge(*vertices[first_index], *vertices[second_index]);
        }
    }
    const auto start = std::chrono::high_resolution_clock::now();
    std::vector<std::vector<std::size_t>> paths;
    paths.reserve(NUM_OF_SHORTEST_PATH_QUERIES);
    for (int i = 0; i < NUM_OF_SHORTEST_PATH_QUERIES; i++) {
        paths.emplace_back(dbEngine.shortestPath(*vertices[distribution(rng)], *vertices[distribution(rng)],
                                                 labels[i % labels.size()]));
    }
    const auto end = std::chrono::high_resolution_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::seconds>(end - start).count();

    return 0;
}
